'''
 	Project: /Beer
 	File: Arduino.py
 	Developer: Michel Bitter
 	Date: july, 21 2014
 	version: 0.1
 	license:
 	Description:
 		Dit bestand 
'''

class arduinoLog():

	def __innet__(self):
		cfg = arduinoConfig()
		control = cfg.issetItem("log", "file");
		if(control == False):
			self.logfile = '/var/log/arduino/arduino.log';
		else:
			self.logfile = cfg.config['log']['file'];

	def errorlog(self, msg):
		from time import gmtime, strftime
		timestamp = strftime("%a, %d %b %Y %H:%M:%S");

		file = open(self.logfile , 'a');
		file.write('ERROR ['+timestamp+']: ' + msg + '\n');
		file.close();

	def infolog(self, msg):
		from time import gmtime, strftime
		timestamp = strftime("%a, %d %b %Y %H:%M:%S");
		file = open(self.logfile , 'a');
		file.write('INFO ['+timestamp+']: ' + msg + '\n');
		file.close();

	def debuglog(self, msg):
		from time import gmtime, strftime
		timestamp = strftime("%a, %d %b %Y %H:%M:%S");
		file = open(self.logfile , 'a');
		file.write('DEBUG ['+timestamp+']: ' + msg + '\n');
		file.close();

class arduinoConfig(arduinoLog):
	error = False;
	config = {'arduino': {}, 'mysql': {}, 'log':{}};

	def __init__(self):
		self.configfile();
		
	def configfile(self):
		from ConfigParser import SafeConfigParser;

		parser = SafeConfigParser()
		configfile = ['config.cfg'];
		found = parser.read(configfile)

		missing = set(configfile) - set(found);
		if(len(missing) == 0):

			for section_name in parser.sections():
			    for name, value in parser.items(section_name):
			        self.config[section_name][name] = parser.get(section_name, name)

		else:
			log = log();
			log.errorlog("Can't open configfile(s): "+missing);
	
	def issetItem(self, section, item):
		try:
		    self.config[section][item];
		except KeyError:
		    self.config[section][item] = "{ERROR}";

		if(self.config[section][item] == "{ERROR}"):
			
			self.errorlog("CONFIG ITEM DOESN'T EXSIST: ["+section+"]["+item+"]");
			self.error = True;
			return False;
		else:
			return True;

class arduinoDatabase(arduinoConfig, arduinoLog):

	def __init__():
		self.configfile();

class arduinoCommunicator(arduinoConfig, arduinoLog, arduinoDatabase):

	def __init__(self):
		self.configfile();
		self.issetItem("arduino", "port");
		self.issetItem("arduino", "bps");

	def readserial(self):
		if(self.error == False):
			import serial;

			ser = serial.Serial(self.config['arduino']['port'], self.config['arduino']['bps']);
			while True:
				if(self.error == False):
					self.readline(ser);
		
	def readline(self, ser):
		readline = ser.readline();
		commtype, data = readline.split(": ");

		if(commtype == "GET"):
			self.get(data);
		elif(commtype == "SET"):
			self.set(data);
		elif(commtype == "ANSWER"):
			self.answer(data);
		elif(commtype == "UPDATE"):
			self.update(data);
		else:
			self.errorlog("RECEIVED UNKNOW COMMTYPE '"+readline+"'")

	def get(data):
		print data;

	def set(data):
		print data;
		
	def answer(data):
		print data;
		
	def update(data):
		print data;


'''
class database(config):
class handshake(config):
'''
		

