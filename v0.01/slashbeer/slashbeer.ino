#include <Wire.h>
#include <EEPROM.h>

#define script_version "0.01"
#define MAX_DISPENSE 50
#define MAX_SENSORS 25
#define SYNTAX_FUNCTION_OPEN 2
#define SYNTAX_FUNCTION_CLOSE 4
#define SYNTAX_MULT_ARGS 8

#define NrOfFunctions 3
// Array with the function names
String CommandList[NrOfFunctions] = {"DispenseWater", "ReadTempSensor","AirReleaseWaterTubing"};
#define BuiltInNrOfFunctions 6
// Array with builtin functions
String BuiltInCommandList[BuiltInNrOfFunctions] = {"HELLO", "VERSION","CMDLIST","CMD","ABORT","RESET"};

// Global variables
String serial, command,commandStr,RetString = ""; //
String argument[10]; // Array for arguments
char inByte; // Byte input from command prompt
int i, Index, ArgsIndex, SendIndex, SyntaxCheck; // temporary numbers
int address,number = 0;
byte value, ExitCode;
char charvalue;
boolean CommandMode;

void setup() {
  Serial.begin(9600);
  Serial.println("Initializing Arduino after reboot");
  Serial.print("Software version: ");
  Serial.println(script_version);

  // first byte in EEPROM is I2C address
  value = EEPROM.read(address);
  
  // initialize i2c as slave
  Wire.begin(value);

  Serial.print("I2C address used by device is ");
  Serial.println(value);
  RetString = ""; CommandMode = true;
  // define callbacks for i2c communication
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  for (int i=1;i<37;i++) {
  // advance to the next address of the EEPROM
  address = address + 1;
 
  // there are only 512 bytes of EEPROM, from 0 to 511, so if we're
  // on address 512, wrap around to address 0
  if (address == 512)
    address = 0;
  charvalue = EEPROM.read(address);
  serial.concat(charvalue); 
  }
  Serial.print("Device with serial ");
  Serial.print(serial);
  Serial.println(" is now ready!");
  ExitCode = 0; SyntaxCheck = 0;
}

void loop() {

}

// callback for received data
void receiveData(int byteCount) { 

  SendIndex = 0; RetString = "";
    
  // Input i2c data:
  while (Wire.available()) {
    inByte = Wire.read();
    //
    // ASCII DEC 40 = (           -> Function start arguments
    //
    // Disable CommandMode, first argument received
    if (inByte == 40) {
      CommandMode = false; if (!(SyntaxCheck & SYNTAX_FUNCTION_OPEN)) { SyntaxCheck += SYNTAX_FUNCTION_OPEN; } else { RetString = "Syntax Error ==> Multiple ("; }
    }
    
    //
    // ASCII DEC 44 = ,           -> Function Field seperator arguments
    //
    // new argument will follow, increase argument index
    if (inByte == 44) {
      CommandMode = false;
      ArgsIndex++;
    }

    //
    // ASCII DEC 32 = <SPACE>
    // ASCII DEC 65-90 = A-Z
    // ASCII DEC 97-122 = a-z
    // ASCII DEC 48-57 = 0-9
    //
    // In CommandMode append incoming character to command string, otherwise append to argument array on current argument index
    if ((inByte >= 65 && inByte <= 90) || (inByte >=97 && inByte <=122) || (inByte >= 48 && inByte <=57) || inByte ==  32) {
      if (CommandMode) {
        command.concat(inByte);
      } else {
        argument[ArgsIndex].concat(inByte);
      }
    }
    
    //
    // ASCII DEC 41 = )           -> Function end arguments
    //
    // function end reached, switch back to Commandmode 
    if (inByte == 41) {
      CommandMode = true; if (!(SyntaxCheck & SYNTAX_FUNCTION_CLOSE)) { SyntaxCheck += SYNTAX_FUNCTION_CLOSE; } else { RetString = "Syntax Error ==> Multiple )"; }
    }
    
    //
    // ASCII DEC 59 = ;           -> EOL
    //
    // End of line reached, process input
    if (inByte == 59) {
      ProcessCommand(command,argument);
    }    
  }// end serial.available
}

// callback for sending data
void sendData(){
  if (SendIndex < Index) { 
    Wire.write(RetString[SendIndex]); 
  } else {
    Wire.write(59);
    RetString = "";
  }
  SendIndex++;
}

void ProcessCommand(String CMD, String args[10]) {
 boolean CommandMatched = false; boolean customCommandMatched = false; RetString = "";
 
 // First check if command is builtin 
 for(i = 0; i < BuiltInNrOfFunctions; i++){
    if(BuiltInCommandList[i] == CMD){
      if (CMD == "HELLO") { RetString = serial; Index = RetString.length(); SendIndex = 0; }
      if (CMD == "VERSION") { RetString = script_version; Index = RetString.length(); SendIndex = 0; }
      CommandMatched = true;
    }
    
    if(CommandMatched) { ClearForNextReceive(); break; } // Stop with for loop as command has been found
 }
 
 if (!CommandMatched) {
  //First do syntax check, initial set SyntaxError to false
  boolean SyntaxError = false; 
  if (!(SyntaxCheck & SYNTAX_FUNCTION_OPEN)) { 
    SyntaxError = true;
    RetString = "Syntax Error ==> ( expected but not found"; 
  }
  if (!(SyntaxCheck & SYNTAX_FUNCTION_CLOSE)) { 
    if (SyntaxError) {
      RetString = "Syntax Error ==> () missing";  
    } else {
      SyntaxError = true; 
      RetString = "Syntax Error ==> ) expected but not found"; 
    }
  }
  //Check custom commands if not builtin
  for(i = 0; i < NrOfFunctions; i++) {
    // If there is a Syntax Error, ignore the command
    if (SyntaxError) { break; }
    // Syntax is okay and check for custom command
    if(CommandList[i] == CMD){
      // if DispenseWater function is called, check if arg1 is number within range 1-MAX_DISPENSE
      if (CMD == "DispenseWater") { 
        int AmountWater = 0;
        AmountWater = args[0].toInt();
        //Serial.print("ARG="); Serial.println(String(AmountWater));
        if (AmountWater > 0 && AmountWater <= MAX_DISPENSE ) {
          RetString = "DispenseWater called to dispense "+String(AmountWater)+" liter of water"; Index = RetString.length(); SendIndex = 0; 
        } else {
          RetString = "Argument out of range"; Index = RetString.length(); SendIndex = 0;
        }
      }
      // if ReadTempSensor function is called, check if arg1 is number within range 1-MAX_SENSORS
      if (CMD == "ReadTempSensor") { 
        int TempSensor = 0;
        TempSensor = args[0].toInt(); 
        if (TempSensor <= MAX_SENSORS ) {
          RetString = "ReadTempSensor called to readout Sensor "+String(TempSensor); Index = RetString.length(); SendIndex = 0;
        } else {
          RetString = "Argument out of range"; Index = RetString.length(); SendIndex = 0;
        }
      }
      // if AirReleaseWaterTubing is called, no arguments are needed
      if (CMD == "AirReleaseWaterTubing") { 
        RetString = "AirReleaseWaterTubing is called"; Index = RetString.length(); SendIndex = 0; 
      }
      
      customCommandMatched = true;
    }
    
    if(customCommandMatched) { ClearForNextReceive(); break; } // Stop with for loop as command has been found
  }
  
  if (SyntaxError) {
    // RetString should already be set with the specified Syntax Error
    ClearForNextReceive();
  } else if (!customCommandMatched) {
    RetString = "Command not found!";
    ClearForNextReceive();
  } 
 }
}

void ClearForNextReceive() {
  SyntaxCheck = 0; command = ""; memset(argument, 0, sizeof(argument)); Index = RetString.length(); SendIndex = 0; CommandMode = true; ArgsIndex = 0;  
}
