# Version 0.1
import smbus
import time
# for RPI version 1, use "bus = smbus.SMBus(0)"
bus = smbus.SMBus(1)

# This is the address we setup in the Arduino Program
address = 0x03

def writeNumber(value):
    # assuming we have an arbitrary size integer passed in value
    for character in str(value): # convert into a string and iterate over it 
	bus.write_byte(address, ord(character)) # send each char's ASCII encoding

    return -1

def readNumber():
	data = ""
	inByte = 0
	while inByte != 59:
		inByte = bus.read_byte(address);
		if (inByte != 59) : data += chr(inByte)
	
	return data;

while True:
    try:
        var = raw_input("Enter 1 - 9: ")
    except ValueError:
        print "Could you at least give me an actual number?"
        continue

    writeNumber(var)
    print "RPI: ", var
    # sleep tenth of a second
    time.sleep(0.33);

    data = readNumber()
    print "Arduino: ", data
