/*
 * EEPROM Write
 *
 * Stores values read from analog input 0 into the EEPROM.
 * These values will stay in the EEPROM when the board is
 * turned off and may be retrieved later by another sketch.
 */

#include <EEPROM.h>

// the current address in the EEPROM (i.e. which byte
// we're going to write to next)
int addr=0;
int i2c_addr=0x04;
String serial="46c6055c-c910-4cd9-bc2c-e6e5be9efea1";

void setup()
{
  // initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.println("Clear EEPROM");
  // write a 0 to all 512 bytes of the EEPROM
  for (int i = 0; i < 512; i++)
    EEPROM.write(i, 0);
  Serial.println(" EEPROM cleared");

  int val=i2c_addr;
 
  // write the value to the appropriate byte of the EEPROM.
  // these values will remain there when the board is
  // turned off.
  EEPROM.write(addr, val);
  Serial.println("I2C address written");
  
  // advance to the next address.  there are 512 bytes in
  // the EEPROM, so go back to 0 when we hit 512.
  for(int i=0;i<serial.length();i++) {
  addr = addr + 1;
  if (addr == 512)
    addr = 0;
  EEPROM.write(addr, byte(serial[i])); 
  Serial.print("Value ");
  Serial.print(serial[i],HEX);
  Serial.print(" is written to addr ");
  Serial.println(addr);
  delay(500);  
  }
  Serial.println("Serial written");
}

void loop() {
}
