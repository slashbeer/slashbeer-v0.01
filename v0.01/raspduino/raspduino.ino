// Testje
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 3
#define TEMPERATURE_PRECISION 9
#define PULSES_PER_LITRE 355
#define NrOfFunctions 3

// Create constructor for FunctionPointers
typedef void (* GenericFP)(void*, ...);

// Predefine functions with variable parameters
void DispenseWater(void* skip, ...);
void AirReleaseWaterTubing(void* skip, ...);
void ReadTempSensor(void* skip, ...);

// Array with the functions used by this sketch
GenericFP CommandFunctions[NrOfFunctions] = {&DispenseWater, &ReadTempSensor, &AirReleaseWaterTubing}; //create an array of 'GenericFP' function pointers.

// Array with the function names
String CommandList[NrOfFunctions] = {"DispenseWater", "ReadTempSensor","AirReleaseWaterTubing"};

// Global variables
String command,commandStr; // Total command string with variables, commandstring without variables
String parameter[5], argument[4]; // temporary string arrays
int CmdWordIndex[5]; // temporary arry for Index
char inByte; // Byte input from command prompt
int i, CmdWordCount,args; // temporary numbers

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress tempDeviceAddress;

int numberOfDevices; // Number of temperature devices found
int flowPin = 2;
int relayPin = 24;
int pulsesCounter = 0;
int pulses = 0;
int totalLitres=0;
String SensorStr;

void rpm ()    
{
  pulsesCounter++;            //RMH every RISING pulse causes pulsesCounter to increase by one.
}

void setup(){
  Serial.begin(9600);
  Serial.println("Initializing Arduino after reboot");
  // initializing code
  pinMode(flowPin, INPUT);
  pinMode(relayPin, OUTPUT);
    // Start up the library
  sensors.begin();
  
  // Grab a count of devices on the wire
  numberOfDevices = sensors.getDeviceCount();
  
  // locate devices on the bus
  Serial.print("Locating temp sensors...");
  
  Serial.print("Found ");
  Serial.print(numberOfDevices, DEC);
  Serial.println(" devices.");

  // Loop through each device, print out address
  for(int i=0;i<numberOfDevices; i++)
  {
    // Search the wire for address
    if(sensors.getAddress(tempDeviceAddress, i))
	{
		Serial.print("Found device ");
		Serial.print(i, DEC);
		Serial.print(" with address: ");
		printAddress(tempDeviceAddress);
		Serial.println();
		
		Serial.print("Setting resolution to ");
		Serial.println(TEMPERATURE_PRECISION, DEC);
		
		// set the resolution to TEMPERATURE_PRECISION bit (Each Dallas/Maxim device is capable of several different resolutions)
		sensors.setResolution(tempDeviceAddress, TEMPERATURE_PRECISION);
		
		Serial.print("Resolution actually set to: ");
		Serial.print(sensors.getResolution(tempDeviceAddress), DEC); 
		Serial.println();
	}else{
		Serial.print("Found ghost device at ");
		Serial.print(i, DEC);
		Serial.print(" but could not detect address. Check power and cabling");
	}
  }
                        
  attachInterrupt(0, rpm, RISING);
  sei();
  
  Serial.println("Ready!");
}

void loop(){
  
  // Input serial information:
  if (Serial.available() > 0){
    inByte = Serial.read();
    // only input if a letter, number, (, ), "," (komma),   are typed!
    if ((inByte >= 65 && inByte <= 90) || (inByte >=97 && inByte <=122) || (inByte >= 48 && inByte <=57) || inByte ==  32) {
      command.concat(inByte);
    }
  }// end serial.available

  // Process command when NL/CR are entered:
  if (inByte == 10 || inByte == 13 || inByte == 59){
        inByte = 0;
        //Serial.println("Command "+command+" received!");

	int LoopCounter = 0; // Counter to use within the while loop
	int IndexCounter = command.indexOf(' '); // Initial fill of IndexCounter
        CmdWordIndex[0] = IndexCounter;
        CmdWordCount = 1;
        int LastIndex = 0;

        // Only search for arguments if FieldSeparator [space] is found
        if(IndexCounter>0) {
        // Find single or multiple argument(s)
	if(IndexCounter != command.lastIndexOf(' ')) {
          // Extract multiple arguments
          while (IndexCounter != command.lastIndexOf(' ') && LoopCounter < 5) { // Keep the loop until the last Index is found or the 4th iteration has taken place
            LastIndex = CmdWordIndex[LoopCounter] + 1;
            LoopCounter++; // Increase the LoopCounter
            CmdWordCount++; // Increate the CmdWord Counter
            IndexCounter = command.indexOf(' ', LastIndex); // Find the next index
            CmdWordIndex[LoopCounter] = IndexCounter; // Set array with index of found space in command string
            if (IndexCounter == command.lastIndexOf(' ')) {
              CmdWordIndex[LoopCounter+1] = command.length(); // Set EOS in last CmdWordIndex
              CmdWordCount++; // Increase counter to include EOS 
            }
          }
        }else{
          // Extract single argument
          LoopCounter++;
          CmdWordIndex[LoopCounter] = command.length(); // Last index is set on end of command string
          CmdWordCount == LoopCounter;  
        }
        }else{
          CmdWordCount = 0; // No arguments present with command, so set CmdWordCount to zero 
        }
        
    commandStr = command.substring(0,CmdWordIndex[0]); // First received word is the command
    //Serial.println("Command received: "+commandStr); 
    //Serial.println("Command word count: "+(String) CmdWordCount);
    
    if(CmdWordCount == 1) {
      argument[1] = command.substring(CmdWordIndex[0]+1,CmdWordIndex[1]);
    }else if(CmdWordCount > 1){
      LastIndex = 0;
      argument[1] = command.substring(CmdWordIndex[0]+1,CmdWordIndex[1]);
      for(i = 1; i < (CmdWordCount-1); i++) {
        argument[i+1] = command.substring(CmdWordIndex[i]+1,CmdWordIndex[i+1]);
      }  
    }
	
    // With the command and arguments set, call the appropiate function
    boolean CommandMatched = false;
    for(i = 0; i < NrOfFunctions; i++){
      if(CommandList[i] == commandStr){
        CommandMatched = true;  // Command has been found
        CommandFunctions[i](NULL); // Call function related to command
      }
      
      if(CommandMatched) { break; } // Stop with for loop as command has been found
    }
    
    if(!CommandMatched){ Serial.println("Unrecognized command! >"+commandStr+"<"); } // Command has not been found
    
    command = "";
    }
    delay(750);
    ReadTempSensor(1);
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

// function to print the temperature for a device
void printTemperature(DeviceAddress deviceAddress, int ID)
{
  Serial.println(sensors.getTempC(deviceAddress));
}

void DispenseWater(void* skip, ...)
{
  va_list args;
  va_start(args, skip);
    
  //Serial.println("DispenseWater function called with argument "+argument[1]+" Liters");
  DispenseWaterFunction(argument[1].toInt());
}

void ReadTempSensor(void* skip, ...)
{
  va_list args;
  va_start(args, skip);
  
  //Serial.println("ReadTempSensor function called with argument "+argument[1]+" as ID");
  ReadTempSensorFunction(argument[1].toInt());
}

void AirReleaseWaterTubing(void* skip, ...)
{
  va_list args;
  va_start(args, skip);
  
  AirReleaseWaterTubingFunction();
}

void AirReleaseWaterTubingFunction()
{
    digitalWrite(relayPin, HIGH);
    delay(10000);
    digitalWrite(relayPin, LOW);
    Serial.println("Air release done");
}

void DispenseWaterFunction(int Amount)
{
  int LoopCount = 0; pulses = 0; pulsesCounter = 0; totalLitres = 0;
  digitalWrite(relayPin, HIGH);
  while(totalLitres < Amount)
  { 
    cli();
    pulses = pulsesCounter;
    sei();
    delay(2);
    if(pulses >= PULSES_PER_LITRE)
    {
      totalLitres++;
      LoopCount++;
      cli();
      pulsesCounter -= PULSES_PER_LITRE;
      sei();
      totalLitres %= 10000;
      //Serial.println(LoopCount);
    }
   }
   digitalWrite(relayPin, LOW); delay(250);
   if(Amount<10)
   {
     Serial.println("0"+(String) Amount+" Liter water dispensed");
   }else if(Amount>10){
     Serial.println((String) Amount+" Liter water dispensed");
   }
}

void ReadTempSensorFunction(int SensorID)
{
  // call sensors.requestTemperatures() to issue a global temperature 
  // request to all devices on the bus
  SensorStr=String(SensorID, DEC);
  //Serial.print("Requesting temperature for Temp sensor ..."+SensorStr+": ");
  sensors.requestTemperatures(); // Send the command to get temperatures
  //Serial.println("DONE");
  
  sensors.getAddress(tempDeviceAddress,SensorID);
  printTemperature(tempDeviceAddress,SensorID);

}


