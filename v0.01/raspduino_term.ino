#include <Wire.h>
#include <EEPROM.h>
#define NrOfFunctions 6
#define script_version "0.01"
String CommandList[NrOfFunctions] = {"HELLO", "VERSION","CMDLIST","CMD","ABORT","RESET"};

// Global variables
String command,commandStr,RetString; // Total command string with variables, commandstring without variables
String parameter[5], argument[4]; // temporary string arrays
int CmdWordIndex[5]; // temporary arry for Index
char inByte; // Byte input from command prompt
int i, Index, SendIndex, CmdWordCount,args; // temporary numbers
int address,number = 0;
byte value;
char charvalue;
String serial;

void setup() {
  Serial.begin(9600);
  Serial.println("Initializing Arduino after reboot");
  Serial.print("Software version: ");
  Serial.println(script_version);

  // first byte in EEPROM is I2C address
  value = EEPROM.read(address);
  
  // initialize i2c as slave
  Wire.begin(value);

  Serial.print("I2C address used by device is ");
  Serial.println(value);
  RetString = "";
  // define callbacks for i2c communication
  Wire.onReceive(receiveData);
  Wire.onRequest(sendData);

  for (int i=1;i<37;i++) {
  // advance to the next address of the EEPROM
  address = address + 1;
 
  // there are only 512 bytes of EEPROM, from 0 to 511, so if we're
  // on address 512, wrap around to address 0
  if (address == 512)
    address = 0;
  charvalue = EEPROM.read(address);
  serial.concat(charvalue); 
  }
  Serial.print("Device with serial ");
  Serial.print(serial);
  Serial.println(" is now ready!");
}

void loop() {

}

// callback for received data
void receiveData(int byteCount) { 

  SendIndex = 0; RetString = "";
  
  // Input i2c data:
  while (Wire.available()) {
    inByte = Wire.read();
    // only input if a letter, number, (, ), "," (komma),   are typed!
    if ((inByte >= 65 && inByte <= 90) || (inByte >=97 && inByte <=122) || (inByte >= 48 && inByte <=57) || inByte ==  32) {
      command.concat(inByte);
    }
  }// end serial.available

  // Process command when NL/CR are entered:
  if (inByte == 10 || inByte == 13 || inByte == 59){
        
        inByte = 0;
        int LoopCounter = 0; // Counter to use within the while loop
	int IndexCounter = command.indexOf(' '); // Initial fill of IndexCounter
        CmdWordIndex[0] = IndexCounter;
        CmdWordCount = 1;
        int LastIndex = 0;

        // Only search for arguments if FieldSeparator [space] is found
        if(IndexCounter>0) {
        // Find single or multiple argument(s)
	if(IndexCounter != command.lastIndexOf(' ')) {
          // Extract multiple arguments
          while (IndexCounter != command.lastIndexOf(' ') && LoopCounter < 5) { // Keep the loop until the last Index is found or the 4th iteration has taken place
            LastIndex = CmdWordIndex[LoopCounter] + 1;
            LoopCounter++; // Increase the LoopCounter
            CmdWordCount++; // Increate the CmdWord Counter
            IndexCounter = command.indexOf(' ', LastIndex); // Find the next index
            CmdWordIndex[LoopCounter] = IndexCounter; // Set array with index of found space in command string
            if (IndexCounter == command.lastIndexOf(' ')) {
              CmdWordIndex[LoopCounter+1] = command.length(); // Set EOS in last CmdWordIndex
              CmdWordCount++; // Increase counter to include EOS 
            }
          }
        }else{
          // Extract single argument
          LoopCounter++;
          CmdWordIndex[LoopCounter] = command.length(); // Last index is set on end of command string
          CmdWordCount == LoopCounter;  
        }
        }else{
          CmdWordCount = 0; // No arguments present with command, so set CmdWordCount to zero 
        }
        
    commandStr = command.substring(0,CmdWordIndex[0]); // First received word is the command
    //Serial.println("Command received: "+commandStr); 
    //Serial.println("Command word count: "+(String) CmdWordCount);
    
    if(CmdWordCount == 1) {
      argument[1] = command.substring(CmdWordIndex[0]+1,CmdWordIndex[1]);
    }else if(CmdWordCount > 1){
      LastIndex = 0;
      argument[1] = command.substring(CmdWordIndex[0]+1,CmdWordIndex[1]);
      for(i = 1; i < (CmdWordCount-1); i++) {
        argument[i+1] = command.substring(CmdWordIndex[i]+1,CmdWordIndex[i+1]);
      }  
    }
	
    // With the command and arguments set, call the appropiate function
    boolean CommandMatched = false;
    for(i = 0; i < NrOfFunctions; i++){
      if(CommandList[i] == commandStr){
        CommandMatched = true;  // Command has been found
        number=i;
        ProcessCommand(commandStr,argument);
      }
      
      if(CommandMatched) { break; } // Stop with for loop as command has been found
    }
    
    if(!CommandMatched){ number=-1; Serial.println("Unrecognized command! >"+commandStr+"<"); Index = 0; SendIndex = 0;} // Command has not been found
    
    command = "";
    }
}

// callback for sending data
void sendData(){
  if (SendIndex < Index) { 
    Serial.print(RetString[SendIndex]);
    Wire.write(RetString[SendIndex]);
  } else {
    Wire.write(59); Serial.println(";");
    RetString = "";
  }
  SendIndex++;
}

void ProcessCommand(String CMD, String args[4]) {
 Serial.print(">");
 Serial.println(CMD);
 if (CMD == "HELLO") { RetString = serial; }
 if (CMD == "VERSION") { RetString = script_version; }
 Index = RetString.length(); SendIndex = 0;
 Serial.print("Return string: "); Serial.println(RetString);
}
